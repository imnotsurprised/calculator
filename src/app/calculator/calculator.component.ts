import {Component, HostListener} from "@angular/core";

@Component({
  selector: 'calculator',
  templateUrl: './calculator.component.html'
})
export class CalculatorComponent {

  public displayValue: string = '0';
  private firstOperand: string = '';
  private secondOperand: string = '';
  private selectedOperator: string = '';
  private operator: string = '';
  private hasProcessedEqual: boolean = false;
  private digitLimit: number = 9;
  private availableKeys: string[] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '/', '*', '-', '+', '.', 'Enter'];
  private availableActions: string[] = ['/', '*', '-', '+'];

  public handleOnClick(button: string): void {
    this.processButton(button);
  }

  @HostListener('window:keypress', ['$event'])
  public handleKeyPress(event: any): void {
    if (this.availableKeys.includes(event.key)) {
      let keyEvent: string = (event.key == 'Enter') ? '=' : event.key;
      this.processButton(keyEvent);
    }
  }

  private processButton(button: string): void {
    if (button == 'C') {
      this.clear();
    }
    else if (button == '=') {
      this.equal();
    }
    else {
      if (this.availableActions.includes(button)) {
        this.selectedOperator = button;
      }
      else {
        this.processNumber(button);
      }
    }
  }

  private processNumber(button: string): void {
    if (this.displayValue == '0' || this.selectedOperator != '') {
      this.displayValue = '';
      this.operator = this.selectedOperator;
      this.selectedOperator = '';
      this.hasProcessedEqual = false;
    }

    if (this.displayValue.length < this.digitLimit) {
      if (button == '+/-') {
        let result:number = -1 * Number(this.displayValue);
        this.displayValue = String(result);
      }
      else if (button == '.') {
        if (!this.displayValue.includes('.')) {
          this.displayValue += (this.displayValue == '') ? '0.' : '.';
        }
      }
      else {
        this.displayValue += button;
      }
    }

    if (this.operator == '') {
      this.firstOperand = this.displayValue;
    }
    else {
      this.secondOperand = this.displayValue;
    }
  }

  private clear(): void {
    this.displayValue = '0';
    this.firstOperand = '';
    this.secondOperand = '';
    this.selectedOperator = '';
    this.hasProcessedEqual = false;
  }

  private equal(): void {
    if (this.operator == '' || this.hasProcessedEqual) {
      return;
    }

    switch (this.operator) {
      case '/':
        if (this.secondOperand == '0') {
          this.clear();
        }
        else {
          let resultDivide: number = parseFloat(this.firstOperand) / parseFloat(this.secondOperand);
          this.displayValue = String(resultDivide);
        }
        break;
      case '*':
        let resultMultiply: number = parseFloat(this.firstOperand) * parseFloat(this.secondOperand);
        this.displayValue = String(resultMultiply.toFixed(this.toFixedMultiplyLength()));
        break;
      case '+':
        let resultAddition: number = parseFloat(this.firstOperand) + parseFloat(this.secondOperand);
        this.displayValue = String(resultAddition.toFixed(this.toFixedLength()));
        break;
      case '-':
        let resultSubtract: number = parseFloat(this.firstOperand) - parseFloat(this.secondOperand);
        this.displayValue = String(resultSubtract.toFixed(this.toFixedLength()));
        break;
    }

    this.firstOperand = this.displayValue;
    this.hasProcessedEqual = true;
    this.operator = '';
  }

  private toFixedMultiplyLength(): number {
    let firstSplit: string[] = this.firstOperand.split('.');
    let secondSplit: string[] = this.secondOperand.split('.');
    let firstLength: number = 0;
    let secondLength: number = 0;

    if (firstSplit[1]) {
      firstLength = firstSplit[1].length;
    }
    if (secondSplit[1]) {
      secondLength = secondSplit[1].length;
    }

    return firstLength + secondLength;
  }

  private toFixedLength(): number {
    let firstSplit = this.firstOperand.split('.');
    let secondSplit = this.secondOperand.split('.');
    let firstLength = 0;
    let secondLength = 0;

    if (firstSplit[1]) {
      firstLength = firstSplit[1].length;
    }
    if (secondSplit[1]) {
      secondLength = secondSplit[1].length;
    }

    return (firstLength > secondLength) ? firstLength : secondLength;
  }
}
